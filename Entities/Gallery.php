<?php

namespace Modules\Gallery\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Photo\Entities\Photo;

/**
 * Class Gallery
 *
 * @package Softici\Gallery
 * @property int $id
 * @property string $name
 * @property int|null $front_photo_id
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Gallery\Entities\Photo[] $photos
 * @property-read int|null $photos_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery whereFrontPhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Gallery\Entities\Gallery whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Gallery extends Model
{
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
}
