<?php


namespace Modules\Gallery\Services;


use Modules\Gallery\Entities\Gallery;
use Modules\Gallery\Entities\Photo;
use Exception;
use Modules\Photo\Services\PhotoService;

class GalleryService
{
    /**
     * Add new photo.
     *
     * @param string $name
     * @param string $description
     * @return Gallery
     */
    public function create(string $name, string $description = null): Gallery
    {
        $gallery = new Gallery();
        $gallery->name = $name;
        $gallery->description = $description;
        $gallery->save();

        return $gallery;
    }

    /**
     * Update gallery.
     *
     * @param Gallery $gallery
     * @param string $name
     * @param string $description
     * @return Gallery
     */
    public function update(Gallery $gallery, string $name, string $description = null): Gallery
    {
        $gallery->name = $name;
        $gallery->description = $description;

        $gallery->save();

        return $gallery;
    }

    /**
     * Delete gallery.
     *
     * @param Gallery $gallery
     * @return bool
     * @throws Exception
     */
    public function delete(Gallery $gallery): bool
    {
        // Delete gallery model
        $gallery->delete();

        // Delete related photos
        $photo_service = new PhotoService();

        foreach ($gallery->photos as $photo)
        {
            $photo_service->delete($photo);
        }

        return true;
    }
}
