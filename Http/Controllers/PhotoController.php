<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Softici\Gallery\Entities\Gallery;
use Softici\Gallery\Entities\Photo;
use Softici\Gallery\Services\PhotoService;

class PhotoController extends Controller
{
    private $photoService;

    public function __construct()
    {
        $this->photoService = new PhotoService();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $gallery = Gallery::findOrFail($request->input('gallery_id'));

        // Create photo
        $this->photoService->create(
            $request->input('name'),
            $request->file('photo_file'),
            $gallery,
            $request->input('description')
        );

        return redirect()->route('galleries.show', $gallery);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $photo = Photo::findOrFail($id);

        return view('admin.photos.edit', [
            'photo' => $photo,
            'galleries' => Gallery::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::findOrFail($request->input('gallery_id'));
        $photo = Photo::findOrFail($id);

        // Create photo
        $this->photoService->update(
            $photo,
            $request->input('name'),
            $gallery,
            $request->input('description')
        );

        return redirect()->route('galleries.show', $gallery);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);

        $this->photoService->delete($photo);

        return redirect()->route('galleries.show', $photo->gallery);
    }
}
