<?php

namespace App\Http\Controllers\Admin;

use Softici\Gallery\Entities\Gallery;
use Softici\Gallery\Services\GalleryService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class GalleryController extends Controller
{
    /**
     * @var GalleryService
     */
    private $galleryService;

    public function __construct()
    {
        $this->galleryService = new GalleryService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('admin.galleries.index', [
            'galleries' => Gallery::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.galleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->galleryService->create($request->input('name'), $request->input('description'));

        return redirect()->route('galleries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function show($id)
    {
        return view('admin.galleries.show', [
            'gallery' => Gallery::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        return view('admin.galleries.edit', [
            'gallery' => Gallery::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Gallery $gallery
     * @return RedirectResponse
     */
    public function update(Request $request, Gallery $gallery)
    {
        $this->galleryService->update($gallery, $request->input('name'), $request->input('description'));

        return redirect()->route('galleries.show', $gallery);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     * @throws Exception
     */
    public function destroy($id)
    {
        $this->galleryService->delete(Gallery::findOrFail($id));
    }
}
